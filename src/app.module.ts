import { Module } from '@nestjs/common';
import { FinancialTransactionModule } from './infra/api/financial-transaction/financial-transaction.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './infra/api/user/user.module';
import { AccountModule } from './infra/api/account/account.module';
console.log(`the uri: ${process.env.DATABASE_URI}`)
@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MongooseModule.forRoot(process.env.DATABASE_URI),
    FinancialTransactionModule,
    UserModule,
    AccountModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
