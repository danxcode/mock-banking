import { Cursor } from "mongoose";
import { IFinancialTransactionRepository } from "../domain/interfaces/repositories/financial-transaction-repo.interface";
import { FinancialTransaction, FinancialTransactionProps } from "../domain/models/financial-transaction.entity";
import { Transform } from "stream";
import { createWriteStream } from "fs";

type Input = {
    dateTo: string;
    datefrom: string
}
type Output = Promise<boolean>;

export class StreamExportTransactionUseCase {
    constructor(
        private readonly financialTransactionRepo: IFinancialTransactionRepository,
    ) {
    }
    execute(input: Input): void {
        const cursor: Cursor = this.financialTransactionRepo.getCursorByDateRange(input.datefrom, input.dateTo);


        const convertToCSVStream = new Transform({
            readableObjectMode: true,
            writableObjectMode: true,
            transform: (chunk, encoding, callback) => {

                const parsed = this.format(chunk)
                callback(null, parsed)
            }
        })
        cursor
            .pipe(convertToCSVStream)
            .pipe(createWriteStream('./stream-csv-exported.csv'))
        cursor.on('end', () => { console.log('Operation finished') })
        return;
    }

    format(transaction: FinancialTransactionProps) {
        try {

            const { debited_account, credited_account, value, created_at, big_hash } = transaction;
            return `${debited_account.number},${credited_account.number},${value},${created_at},${big_hash};\n`;
        }
        catch (error) {
            console.error(error)
            return false
        }
    }
}