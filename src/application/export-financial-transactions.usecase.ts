import { IFileProvider } from "../domain/interfaces/providers/file-provider.interface";
import { IFinancialTransactionRepository } from "../domain/interfaces/repositories/financial-transaction-repo.interface";
import { FinancialTransaction } from "../domain/models/financial-transaction.entity";

type Input = {
    dateTo: string;
    datefrom: string
}
type Output = Promise<boolean>;
export class ExportFinancialTransactionUseCase {

    constructor(
        private readonly financialTransactionRepo: IFinancialTransactionRepository,
        private readonly fileProvider: IFileProvider<FinancialTransaction>
    ) {
    }

    async execute(input: Input): Output {
        try {
            const { dateTo, datefrom } = input;
            const financialTransactions = await this.financialTransactionRepo.getByDateRange(datefrom, dateTo);
            this.fileProvider.export(financialTransactions, './csv-exported.csv')
            return true;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
}