import { IAccountRepository } from "../domain/interfaces/repositories/account-repo.interface";
import { ICreateAccountUseCase } from "../domain/interfaces/usecases/create-account.usecase.interface";
import { AccountProps } from "../domain/models/account.entity";

export class CreateAccountUseCase implements ICreateAccountUseCase {
    constructor(private readonly accountRepo: IAccountRepository) { }
    async execute(props: AccountProps): Promise<boolean> {
        const isUnique = await this.accountRepo.isUnique(props);
        if (!isUnique) return false;
        const account = await this.accountRepo.create(props);
        return !!account;
    }

}