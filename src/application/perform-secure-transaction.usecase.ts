import { IAccountRepository } from "../domain/interfaces/repositories/account-repo.interface";
import { IFinancialSecureTransactionRepository } from "../domain/interfaces/repositories/financial-transaction-repo.interface";
import { IPerformTransactionUseCase } from "../domain/interfaces/usecases/peform-transaction.usecase.interface";
import { FinancialTransaction, FinancialTransactionProps } from "../domain/models/financial-transaction.entity";

export class PerformSecureTransactionUseCase implements IPerformTransactionUseCase {
    constructor(
        private readonly financialTransactionRepository: IFinancialSecureTransactionRepository,
        private readonly accountRepository: IAccountRepository
    ) { }
    async execute(financialTransactionProps: FinancialTransactionProps): Promise<boolean> {
        try {
            const { credited_account: credited_account_props, debited_account: debited_account_props } = financialTransactionProps
            const fdebited_account = await this.accountRepository.findOne({ id: debited_account_props?.id, number: debited_account_props?.number })
            const fcredited_account = await this.accountRepository.findOne({ id: credited_account_props?.id, number: credited_account_props?.number })
            financialTransactionProps.credited_account = fcredited_account.props;
            financialTransactionProps.debited_account = fdebited_account.props;
            const financialTransaction = new FinancialTransaction(financialTransactionProps);
            const { credited_account, debited_account } = financialTransaction.transfer();
            return await this.financialTransactionRepository.createWithTransaction(debited_account, credited_account, financialTransaction.props);
        } catch (error) {
            throw error
        }
    }
}