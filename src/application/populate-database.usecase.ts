import { IAccountRepository } from "../domain/interfaces/repositories/account-repo.interface";
import { IFinancialTransactionRepository } from "../domain/interfaces/repositories/financial-transaction-repo.interface";
import { faker } from '@faker-js/faker';
import { AccountProps, AccountType } from "../domain/models/account.entity";
import { User } from "../domain/models/user.entity";
import { IUserRepository } from "../domain/interfaces/repositories/user-repo.interface";
import { FinancialTransaction } from "../domain/models/financial-transaction.entity";
import * as moment from "moment";

export class PopulateDatabaseUseCase {
    constructor(
        private readonly financialTransactionRepository: IFinancialTransactionRepository,
        private readonly accountRepository: IAccountRepository,
        private readonly userRepository: IUserRepository,
    ) { }
    async execute(input: { account_qtd: number, transaction_per_account_qtd: number, transactions_date: string }) {
        const { account_qtd, transaction_per_account_qtd, transactions_date } = input;
        for (let i = 0; i < account_qtd; i++) {
            const user = new User({ email: faker.internet.email(), name: faker.person.fullName() })
            const u = await this.userRepository.create(user.props);
            const acc: AccountProps =
            {
                balance: faker.number.int({ min: 1000, max: 1000000 }),
                number: i.toString().padStart(8, '0'),
                type: AccountType.NATIONAL,
                userId: u.props.id,
                user: undefined
            }
            await this.accountRepository.create(acc)
        }

        const accounts = await this.accountRepository.getAll()
        for (let i = 0; i < accounts.length; i++) {
            for (let j = 0; j < transaction_per_account_qtd; j++) {
                let credited_acc_index = faker.number.int({ min: 0, max: accounts.length - 1 })
                while (credited_acc_index == i) {
                    credited_acc_index = faker.number.int({ min: 0, max: accounts.length - 1 })
                }
                const c_account = accounts[credited_acc_index];
                const d_account = accounts[i];
                const financialTransaction = new FinancialTransaction({
                    big_hash: "#hash@".repeat(1024 * 5),
                    created_at: moment(transactions_date).toDate(),
                    credited_account: c_account.props,
                    debited_account: d_account.props,
                    value: 100
                });
                const { credited_account, debited_account } = financialTransaction.transfer();
                await this.accountRepository.update(credited_account);
                await this.accountRepository.update(debited_account);
                await this.financialTransactionRepository.create(financialTransaction.props);
            }
        }

        return true;
    }
}