import { IUserRepository } from "../domain/interfaces/repositories/user-repo.interface";
import { ICreateUserUseCase } from "../domain/interfaces/usecases/create-user.usecase.interface";
import { User, UserProps } from "../domain/models/user.entity";

export class CreateUserUseCase implements ICreateUserUseCase {
    constructor(private readonly userRepository: IUserRepository) { }
    async execute(props: UserProps): Promise<boolean> {
        User.mapToEntity(props);
        const user = await this.userRepository.create(props);
        return !!user
    }

}