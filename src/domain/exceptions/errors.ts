export class DomainError extends Error {
    statusCode = 500;
    constructor(message: string) {
        super(message);
        Object.setPrototypeOf(this, DomainError.prototype);
    }
    getErrorMessage() {
        return 'Domain Error: ' + this.message;
    }
}