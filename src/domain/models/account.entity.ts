import { User } from "./user.entity";

export enum AccountType {
    INTERNATIONAL,
    NATIONAL
}

export type AccountProps = {
    id?: string;
    number: string;
    balance: number;
    userId: string;
    user: User;
    type: AccountType;
}

export class Account {
    constructor(public props: AccountProps) {
        this.props = props;
    }
    static mapToEntity(props: AccountProps): Account {
        return new Account(props);
    }
}