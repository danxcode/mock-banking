import { DomainError } from "../exceptions/errors";
import { AccountProps } from "./account.entity"

export type FinancialTransactionProps = {
    debited_account: Partial<AccountProps>;
    credited_account: Partial<AccountProps>;
    value: number;
    created_at: Date;
    big_hash: string;
}
export class FinancialTransaction {

    constructor(public props: FinancialTransactionProps) {
        this.props = props
    }

    static mapToEntity(props: FinancialTransactionProps) {
        return new FinancialTransaction(props)
    }

    transfer() {
        const { credited_account, debited_account, value } = this.props;
        if (debited_account.balance < value)
            throw new DomainError(`The account n° ${debited_account.number} have insuficient balance`);
        if (debited_account.type !== credited_account.type)
            throw new DomainError(`The accounts aren't the same type`);
        if (debited_account.number === credited_account.number)
            throw new DomainError(`The accounts have the same number`);

        credited_account.balance += value;
        debited_account.balance -= value;

        return { credited_account, debited_account }
    }
}