export abstract class BaseEntity<Props, Entity>{
    abstract mapToEntity(props: Props): Entity;
}