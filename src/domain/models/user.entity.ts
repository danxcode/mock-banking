export type UserProps = {
    id?: string;
    name: string;
    email: string;
}

export class User {
    constructor(public props: UserProps) {
        this.props = props
    }
    static mapToEntity(props: UserProps) {
        return new User(props)
    }
}