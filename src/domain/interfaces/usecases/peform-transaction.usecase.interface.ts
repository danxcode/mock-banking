import { FinancialTransactionProps } from "../../models/financial-transaction.entity";

export interface IPerformTransactionUseCase {
    execute(financialTransaction: FinancialTransactionProps): Promise<boolean>
}