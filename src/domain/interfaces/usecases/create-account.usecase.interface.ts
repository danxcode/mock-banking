import { AccountProps } from "../../models/account.entity";

export interface ICreateAccountUseCase {
    execute(props: AccountProps): Promise<boolean>
}