import { UserProps } from "../../models/user.entity";

export interface ICreateUserUseCase {
    execute(user: UserProps): Promise<boolean>
}