export interface IFileProvider<T> {
    export(data: T[], path: string): Promise<boolean>
}