import { Account, AccountProps } from "../../models/account.entity"

export interface IAccountRepository {
    create(props: AccountProps): Promise<Account>
    findOne(props: { id: string, number: string }): Promise<Account>
    getAll(): Promise<Account[]>
    createBulk(array_props: AccountProps[]): Promise<void>
    isUnique(props: AccountProps): Promise<boolean>
    update(props: Partial<AccountProps>): Promise<boolean>
}
