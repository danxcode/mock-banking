import { User, UserProps } from "../../models/user.entity"

export interface IUserRepository {
    create(props: UserProps): Promise<User>
    createBulk(peoplePropertiers: UserProps[]): Promise<void>
}
