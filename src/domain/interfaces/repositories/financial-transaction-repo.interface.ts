import { AccountProps } from "../../models/account.entity"
import { FinancialTransaction, FinancialTransactionProps } from "../../models/financial-transaction.entity"

export interface IFinancialTransactionRepository {
    create(props: FinancialTransactionProps): Promise<FinancialTransaction>
    createBulk(peoplePropertiers: FinancialTransactionProps[]): Promise<void>
    getByDateRange(dateFrom: string, dateTo: string): Promise<FinancialTransaction[]>
    getCursorByDateRange(dateFrom: string, dateTo: string): any
}

export interface IFinancialSecureTransactionRepository {
    createWithTransaction(
        debited_account: Partial<AccountProps>,
        credited_account: Partial<AccountProps>,
        financialTransactionProps: FinancialTransactionProps): Promise<boolean>
}
