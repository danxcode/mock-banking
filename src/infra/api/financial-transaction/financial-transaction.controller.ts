import { Body, Controller, Get, Post } from "@nestjs/common";
import { PerformTransactionUseCase } from "../../../application/peform-transaction.usecase";
import { CreateTransactionDTO } from "./dtos/create-transaction.dto";
import { PerformSecureTransactionUseCase } from "../../../application/perform-secure-transaction.usecase";
import { PopulateDBDTO } from "./dtos/populate-db.dto";
import { PopulateDatabaseUseCase } from "../../../application/populate-database.usecase";
import { ExportFinancialTransactionUseCase } from "../../../application/export-financial-transactions.usecase";
import { ExportTransactionDTO } from "./dtos/export-transactions.dto";
import { StreamExportTransactionUseCase } from "../../../application/stream-export-financial-transaction.usecase";

@Controller('financial-transaction')
export class FinancialTransactionController {
    constructor(
        private readonly performTransactionUseCase: PerformTransactionUseCase,
        private readonly performSecureTransactionUseCase: PerformSecureTransactionUseCase,
        private readonly populateDatabaseUseCase: PopulateDatabaseUseCase,
        private readonly exportFinancialTransactionUseCase: ExportFinancialTransactionUseCase,
        private readonly streamExportTransactionUseCase: StreamExportTransactionUseCase,
    ) {

    }
    @Get('/')
    async getAllTransactions() {
        return { ok: true }
    }

    @Post('/transfer')
    async performTransfer(@Body() createTransactionDTO: CreateTransactionDTO) {
        return await this.performTransactionUseCase.execute(createTransactionDTO)
    }

    @Post('/secure-transfer')
    async performSecureTransfer(@Body() createTransactionDTO: CreateTransactionDTO) {
        return await this.performSecureTransactionUseCase.execute(createTransactionDTO)
    }

    @Post('/populate')
    async populate(@Body() populateDBDTO: PopulateDBDTO) {
        return await this.populateDatabaseUseCase.execute(populateDBDTO)
    }

    @Post('/export')
    async export(@Body() exportTransactionDTO: ExportTransactionDTO) {
        return await this.exportFinancialTransactionUseCase.execute(exportTransactionDTO)
    }

    @Post('/export-stream')
    async exportStream(@Body() exportTransactionDTO: ExportTransactionDTO) {
        return await this.streamExportTransactionUseCase.execute(exportTransactionDTO)
    }
}