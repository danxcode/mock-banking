import { Module } from '@nestjs/common';
import { FinancialTransactionController } from './financial-transaction.controller';
import { PerformTransactionUseCase } from '../../../application/peform-transaction.usecase';
import { MongooseModule } from '@nestjs/mongoose';
import { AccountSchema, MongooseAccount } from '../../repositories/schemas/mongoose-account.schema';
import { MongooseAccountRepository } from '../../repositories/mongoose-account.repository';
import { FinancialTransactionSchema, MongooseFinancialTransaction } from '../../repositories/schemas/mongoose-financial-transaction.schema';
import { MongooseSecureTransactionRepository } from '../../repositories/mongoose-secure-transaction';
import { PerformSecureTransactionUseCase } from '../../../application/perform-secure-transaction.usecase';
import { PopulateDatabaseUseCase } from '../../../application/populate-database.usecase';
import { MongooseUser, UserSchema } from '../../repositories/schemas/mongoose-user.schema';
import { MongooseUserRepository } from '../../repositories/mongoose-user.repository';
import { ExportFinancialTransactionUseCase } from '../../../application/export-financial-transactions.usecase';
import { CSVTransactionFileProvider } from '../../providers/csv-transaction-file.provider';
import { StreamExportTransactionUseCase } from '../../../application/stream-export-financial-transaction.usecase';
import { FinancialTransactionIndexedSchema, MongooseFinancialTransactionIndexed } from '../../repositories/schemas/mongoose-financial-transaction-indexed.schema';
import { MongooseFinancialTransactionIndexedRepository } from '../../repositories/mongoose-financial-transaction-indexed.repository';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: MongooseAccount.name, schema: AccountSchema }]),
        MongooseModule.forFeature([{ name: MongooseFinancialTransaction.name, schema: FinancialTransactionSchema }]),
        MongooseModule.forFeature([{ name: MongooseFinancialTransactionIndexed.name, schema: FinancialTransactionIndexedSchema }]),
        MongooseModule.forFeature([{ name: MongooseUser.name, schema: UserSchema }]),
    ],
    providers: [
        { provide: 'UserRepository', useClass: MongooseUserRepository },
        { provide: 'TransactionFileProvider', useClass: CSVTransactionFileProvider },
        { provide: 'AccountRepository', useClass: MongooseAccountRepository },
        { provide: 'FinancialTransactionRepository', useClass: MongooseFinancialTransactionIndexedRepository },
        { provide: 'MongooseFinancialTransactionIndexedRepository', useClass: MongooseFinancialTransactionIndexedRepository },
        { provide: 'SecureTransactionRepository', useClass: MongooseSecureTransactionRepository },
        {
            provide: PerformTransactionUseCase, useFactory: (ftRepo, accountRepo) => {
                return new PerformTransactionUseCase(ftRepo, accountRepo);
            },
            inject: ['FinancialTransactionRepository', 'AccountRepository']
        },
        {
            provide: PerformSecureTransactionUseCase, useFactory: (ftRepo, accountRepo) => {
                return new PerformSecureTransactionUseCase(ftRepo, accountRepo);
            },
            inject: ['SecureTransactionRepository', 'AccountRepository']
        },
        {
            provide: PopulateDatabaseUseCase, useFactory: (ftRepo, accountRepo, userRepo) => {
                return new PopulateDatabaseUseCase(ftRepo, accountRepo, userRepo);
            },
            inject: ['FinancialTransactionRepository', 'AccountRepository', 'UserRepository']
        },
        {
            provide: ExportFinancialTransactionUseCase, useFactory: (ftRepo, fileProvider) => {
                return new ExportFinancialTransactionUseCase(ftRepo, fileProvider)
            },
            inject: ['FinancialTransactionRepository', 'TransactionFileProvider']
        },
        {
            provide: StreamExportTransactionUseCase, useFactory: (ftRepo) => {
                return new StreamExportTransactionUseCase(ftRepo)
            },
            inject: ['FinancialTransactionRepository']
        }
    ],
    controllers: [FinancialTransactionController]
})
export class FinancialTransactionModule { }
