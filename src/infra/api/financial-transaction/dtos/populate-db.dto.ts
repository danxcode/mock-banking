import { IsDateString, Max, Min } from "class-validator";

export class PopulateDBDTO {
    @Min(0)
    @Max(10000)
    account_qtd: number;

    @Min(1)
    @Max(100000)
    transaction_per_account_qtd: number


    @IsDateString()
    transactions_date: string
}
