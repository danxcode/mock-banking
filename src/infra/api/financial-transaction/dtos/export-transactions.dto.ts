import { IsDateString } from "class-validator";

export class ExportTransactionDTO {
    @IsDateString()
    dateTo: string;
    @IsDateString()
    datefrom: string
}