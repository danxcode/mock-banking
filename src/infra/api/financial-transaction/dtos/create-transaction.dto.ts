import { IsString, Min } from "class-validator";
import { Account, AccountProps } from "../../../../domain/models/account.entity";

export class CreateTransactionDTO {
    debited_account: Partial<AccountProps>;
    credited_account: Partial<AccountProps>;
    @Min(0)
    value: number;
    created_at: Date;
    @IsString()
    big_hash: string;
}
