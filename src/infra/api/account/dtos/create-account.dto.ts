import { IsEnum, IsNumberString, IsString, MaxLength, Min, MinLength } from "class-validator";
import { User } from "../../../../domain/models/user.entity";
import { AccountType } from "../../../../domain/models/account.entity";

export class CreateAccountDto {


    @IsNumberString()
    @MaxLength(8)
    @MinLength(8)
    number: string;

    @Min(0)
    balance: number;

    user: User;

    @IsString()
    userId: string;

    @IsEnum(AccountType)
    type: AccountType;
}
