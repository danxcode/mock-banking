import { Controller, Post, Body, BadRequestException } from '@nestjs/common';
import { CreateAccountDto } from './dtos/create-account.dto';
import { CreateAccountUseCase } from '../../../application/create-account.usecase';

@Controller('account')
export class AccountController {
    constructor(private createAccountUseCase: CreateAccountUseCase) { }

    @Post()
    async create(@Body() createUserDto: CreateAccountDto) {
        const created = await this.createAccountUseCase.execute(createUserDto);
        if (!created) throw new BadRequestException("This account already exists")
        return created
    }

}
