import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AccountSchema, MongooseAccount } from "../../repositories/schemas/mongoose-account.schema";
import { AccountController } from "./account.controller";
import { MongooseAccountRepository } from "../../repositories/mongoose-account.repository";
import { CreateAccountUseCase } from "../../../application/create-account.usecase";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: MongooseAccount.name, schema: AccountSchema }]),
    ],
    providers: [
        { provide: 'AccountRepository', useClass: MongooseAccountRepository },
        {
            provide: CreateAccountUseCase, useFactory: (accountRepo) => {
                return new CreateAccountUseCase(accountRepo)
            },
            inject: ['AccountRepository']
        }
    ],
    controllers: [AccountController]
})
export class AccountModule { }