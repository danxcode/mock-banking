import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { CreateUserUseCase } from '../../../application/create-user.usecase';
import { MongooseModule } from '@nestjs/mongoose';
import { MongooseUser, UserSchema } from '../../repositories/schemas/mongoose-user.schema';
import { MongooseUserRepository } from '../../repositories/mongoose-user.repository';
import { IUserRepository } from '../../../domain/interfaces/repositories/user-repo.interface';

@Module({
  imports: [MongooseModule.forFeature([{ name: MongooseUser.name, schema: UserSchema }])],
  controllers: [UserController],
  providers: [{
    provide: CreateUserUseCase, useFactory: (userRepository: IUserRepository) => {
      return new CreateUserUseCase(userRepository);
    },
    inject: ['UserRepository']
  },
  { provide: 'UserRepository', useClass: MongooseUserRepository }


  ]
})
export class UserModule { }
