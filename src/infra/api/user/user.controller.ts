import { Controller, Post, Body } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { CreateUserUseCase } from '../../../application/create-user.usecase';

@Controller('user')
export class UserController {
  constructor(private createUserUseCase: CreateUserUseCase) { }

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    await this.createUserUseCase.execute(createUserDto);
  }

}
