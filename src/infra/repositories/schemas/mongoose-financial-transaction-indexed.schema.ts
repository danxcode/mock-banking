import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { MongooseAccount } from "./mongoose-account.schema";

@Schema({ collection: "financial-transactions-indexed" })
export class MongooseFinancialTransactionIndexed {
    @Prop({ type: MongooseAccount })
    debited_account: Partial<MongooseAccount>;
    @Prop({ type: MongooseAccount })
    credited_account: Partial<MongooseAccount>;
    @Prop({ type: Number, min: 1 })
    value: number;
    @Prop()
    created_at: Date;
    @Prop()
    big_hash: string;
}

export type FinancialTransactionIndexedDocument = HydratedDocument<MongooseFinancialTransactionIndexed>;

const FinancialTransactionIndexedSchema = SchemaFactory.createForClass(MongooseFinancialTransactionIndexed);
FinancialTransactionIndexedSchema.index({ created_at: 1 }, { unique: false });
export { FinancialTransactionIndexedSchema };