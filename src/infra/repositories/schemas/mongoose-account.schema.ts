import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { User } from "../../../domain/models/user.entity";
import { AccountType } from "../../../domain/models/account.entity";
import { HydratedDocument, Schema as MongooseSchema } from "mongoose";

@Schema({ collection: "accounts" })
export class MongooseAccount {
    @Prop()
    id: string;
    @Prop({
        type: String, minlength: 8, maxlength: 8, validate: (v: string) => {
            /^\d+$/.test(v);
        }
    })
    number: string;
    @Prop({ type: Number, min: 0 })
    balance: number;
    @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'User' })
    user: User;

    @Prop()
    userId: string;
    @Prop({ type: String, enum: AccountType })
    type: AccountType;
}

export type AccountDocument = HydratedDocument<MongooseAccount>;
export const AccountSchema = SchemaFactory.createForClass(MongooseAccount);