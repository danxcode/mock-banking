import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<MongooseUser>;

@Schema({ collection: "users" })
export class MongooseUser {
    @Prop()
    name: string;

    @Prop()
    email: string;
}

export const UserSchema = SchemaFactory.createForClass(MongooseUser);