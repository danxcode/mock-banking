import { InjectModel } from "@nestjs/mongoose";
import { IAccountRepository } from "../../domain/interfaces/repositories/account-repo.interface";
import { AccountProps, Account } from "../../domain/models/account.entity";
import { Model } from "mongoose";
import { AccountDocument, MongooseAccount } from "./schemas/mongoose-account.schema";

export class MongooseAccountRepository implements IAccountRepository {
    constructor(@InjectModel(MongooseAccount.name) private model: Model<AccountDocument>) { }
    async getAll(): Promise<Account[]> {
        const accounts = await this.model.find({});
        return accounts.map(a => Account.mapToEntity(a));
    }
    async findOne(props: { id: string; number: string; }): Promise<Account> {
        const { id, number } = props;
        if (id) {
            const account = await this.model.findById(id);
            return Account.mapToEntity(account)
        }
        if (number) {
            const account = await this.model.findOne({ number });
            return Account.mapToEntity(account)
        }
        return
    }
    async update(props: Partial<AccountProps>): Promise<boolean> {


        const updated = await this.model.updateOne({ number: props.number }, {
            $set: {
                ...props
            }
        })
        return !!updated
    }
    async create(props: AccountProps): Promise<Account> {
        const account = await this.model.create(props);
        return Account.mapToEntity(account);
    }
    async createBulk(array_props: AccountProps[]): Promise<void> {
        await this.model.insertMany(array_props);
    }
    async isUnique(props: AccountProps): Promise<boolean> {
        const alreadyExists = await this.model.findOne({ number: props.number });
        return !alreadyExists;
    }

}