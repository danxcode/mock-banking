import { InjectConnection, InjectModel } from "@nestjs/mongoose";
import { IFinancialSecureTransactionRepository } from "../../domain/interfaces/repositories/financial-transaction-repo.interface";
import { AccountProps } from "../../domain/models/account.entity";
import { FinancialTransactionProps } from "../../domain/models/financial-transaction.entity";
import { FinancialTransactionDocument, MongooseFinancialTransaction } from "./schemas/mongoose-financial-transaction.schema";
import { Connection, Model } from "mongoose";
import { AccountDocument, MongooseAccount } from "./schemas/mongoose-account.schema";



export class MongooseSecureTransactionRepository implements IFinancialSecureTransactionRepository {

    constructor(
        @InjectModel(MongooseFinancialTransaction.name) private model: Model<FinancialTransactionDocument>,
        @InjectModel(MongooseAccount.name) private accountModel: Model<AccountDocument>,
        @InjectConnection() private readonly connection: Connection,
    ) {

    }
    async createWithTransaction(
        debited_account: Partial<AccountProps>,
        credited_account: Partial<AccountProps>,
        financialTransactionProps: FinancialTransactionProps) {
        let transactionCompleted = false;
        const session = await this.connection.startSession();
        session.startTransaction();
        try {
            const credited_account_to_update = await this.accountModel.findOne({ number: credited_account.number })
            credited_account_to_update.balance = credited_account.balance;
            await credited_account_to_update.save({ session });

            const debited_account_to_update = await this.accountModel.findOne({ number: debited_account.number })
            debited_account_to_update.balance = debited_account.balance;
            await debited_account_to_update.save({ session });
            (await this.model.create([{
                ...financialTransactionProps
            }], { session }))[0];
            session.commitTransaction();
            transactionCompleted = true;
        } catch (error) {
            await session.abortTransaction();
            transactionCompleted = false;

        } finally {
            session.endSession();
        }
        return transactionCompleted;
    }
}