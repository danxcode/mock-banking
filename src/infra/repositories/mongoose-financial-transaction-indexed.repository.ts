import { InjectModel } from "@nestjs/mongoose";
import { IFinancialTransactionRepository } from "../../domain/interfaces/repositories/financial-transaction-repo.interface";
import { FinancialTransactionProps, FinancialTransaction } from "../../domain/models/financial-transaction.entity";
import { Model } from "mongoose";
import * as moment from "moment";
import { FinancialTransactionIndexedDocument, MongooseFinancialTransactionIndexed } from "./schemas/mongoose-financial-transaction-indexed.schema";

export class MongooseFinancialTransactionIndexedRepository implements IFinancialTransactionRepository {
    constructor(
        @InjectModel(MongooseFinancialTransactionIndexed.name) private model: Model<FinancialTransactionIndexedDocument>,
    ) { }
    getCursorByDateRange(dateFrom: string, dateTo: string) {
        return this.model.find({ created_at: { $gte: moment(dateFrom).toDate(), $lt: moment(dateTo).toDate() } }).cursor()
    }
    async getByDateRange(dateFrom: string, dateTo: string): Promise<FinancialTransaction[]> {
        const stats = await this.model.find({ created_at: { $gte: moment(dateFrom).toDate(), $lt: moment(dateTo).toDate() } }).explain()
        const financialTransacations = await this.model.find({ created_at: { $gte: moment(dateFrom).toDate(), $lt: moment(dateTo).toDate() } }).lean()
        return financialTransacations.map(ft => FinancialTransaction.mapToEntity(ft))
    }
    async create(props: FinancialTransactionProps): Promise<FinancialTransaction> {
        const ft = await this.model.create(props);
        return FinancialTransaction.mapToEntity(props);
    }
    createBulk(peoplePropertiers: FinancialTransactionProps[]): Promise<void> {
        throw new Error("Method not implemented.");
    }

}