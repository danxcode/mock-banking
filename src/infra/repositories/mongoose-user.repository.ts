import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { IUserRepository } from '../../domain/interfaces/repositories/user-repo.interface';
import { User, UserProps } from '../../domain/models/user.entity';
import { MongooseUser, UserDocument } from './schemas/mongoose-user.schema';


export class MongooseUserRepository implements IUserRepository {
    constructor(
        @InjectModel(MongooseUser.name)
        private model: Model<UserDocument>,
    ) { }
    async createBulk(peoplePropertiers: UserProps[]): Promise<void> {
        await this.model.collection.insertMany(peoplePropertiers)
    }

    async create(props: UserProps): Promise<User> {
        const user = await this.model.create(props);

        return User.mapToEntity(user)
    }

    async find(filter: FilterQuery<UserDocument>, options: any): Promise<User[]> {
        const users = await this.model.find(filter, options)
        return users.map(user => User.mapToEntity(user))
    }

    findStream(filter: FilterQuery<UserDocument>, options: any) {
        return this.model.find<UserDocument>(filter, options).cursor()
    }


}
