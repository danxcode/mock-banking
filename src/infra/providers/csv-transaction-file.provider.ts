import { writeFile } from "fs";
import { IFileProvider } from "../../domain/interfaces/providers/file-provider.interface";
import { FinancialTransaction } from "../../domain/models/financial-transaction.entity";

export class CSVTransactionFileProvider implements IFileProvider<FinancialTransaction>{
    async export(data: FinancialTransaction[], path: string): Promise<boolean> {
        try {
            let rawCSVData = 'debited_account,credited_account,value,created_at,big_hash;\n';

            data.forEach(transaction => {
                const { debited_account, credited_account, value, created_at, big_hash } = transaction.props;
                rawCSVData += `${debited_account.number},${credited_account.number},${value},${created_at},${big_hash};\n`
            })
            writeFile(path, rawCSVData, (err) => { if (err) console.error(err) })
            return true
        } catch (error) {
            console.error(error)
            return false
        }
    }

}